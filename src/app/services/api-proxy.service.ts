import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ApiProxyService {
  apiWeather = 'http://api.openweathermap.org/data/2.5/';
  cryptoApi = 'https://api.coinmarketcap.com/v1/ticker/?limit=10';
  constructor(private http: HttpClient) { }

  getCities() {
    return this.http.get('assets/city.list.json');
  }

  getCurrentWeather(id: number) {
    return this.http.get(this.apiWeather + 'weather?id=' + id);
  }

  getHistoryWeather(id: number) {
    return this.http.get(this.apiWeather + 'forecast?id=' + id);
  }

  getCryptos() {
    return this.http.get(this.cryptoApi);
  }
}
