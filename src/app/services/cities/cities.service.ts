import { ApiProxyService } from './../api-proxy.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  constructor(private service: ApiProxyService) { }

  searchCity(name: string) {
    return this.service.getCities()
                        .pipe(map(
                              (response: any) => {
                                return response.filter(city => city.name.toLowerCase().includes(name.toLowerCase()));
                              }));
  }
}
