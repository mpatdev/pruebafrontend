import { ApiProxyService } from './../api-proxy.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor(private service: ApiProxyService) { }

  getListCrypto() {
    return this.service.getCryptos().pipe(map(res => res));
  }
}
