import { ApiProxyService } from './../api-proxy.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private service: ApiProxyService) { }

  getWeather(id: number) {
    return this.service.getCurrentWeather(id).pipe(map(data => data));
  }

  getHistoric(id: number) {
    return this.service.getHistoryWeather(id).pipe(map(data => data));
  }
}
