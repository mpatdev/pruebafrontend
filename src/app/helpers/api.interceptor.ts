import { environment } from '../../environments/environment';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpParams,
    HttpRequest
    } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()

export class WeatherApiInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.includes('api.openweathermap.org')) {
            req = req.clone({ url: `${req.url}&appid=${environment.weatherKey}&units=metric&lang=es`});
        }
        return next.handle(req);
    }
}
