import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iconWeather'
})
export class IconWeatherPipe implements PipeTransform {

  transform(icon: string ): string {
    return 'https://openweathermap.org/img/w/' + icon + '.png';
  }

}
