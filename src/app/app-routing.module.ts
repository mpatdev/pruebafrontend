import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { CryptoComponent } from './components/crypto/crypto.component';
import { NgModule } from '@angular/core';
import { PwnieComponent } from './components/pwnie/pwnie.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'weather-in-city/:id', component: CityWeatherComponent},
  {path: 'crypto', component: CryptoComponent},
  {path: 'pwnie', component: PwnieComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
