export interface Crypto {
    id: string;
    name: string;
    symbol: string;
    rank: string;
    price_usd: string;
    price_btc: string;
    percent_change_1h: string;
    percent_change_24h: string;
    percent_change_7d: string;
    last_update: string;
}
