import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IconWeatherPipe } from './pipes/icon-weather.pipe';
import { MaterialExportModule } from './app-material.module';
import { MomentModule } from 'ngx-moment';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './components/search/search.component';
import { WeatherApiInterceptor } from './helpers/api.interceptor';
import { CryptoComponent } from './components/crypto/crypto.component';
import { PwnieComponent } from './components/pwnie/pwnie.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    CityWeatherComponent,
    IconWeatherPipe,
    CryptoComponent,
    PwnieComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialExportModule,
    HttpClientModule,
    ReactiveFormsModule,
    MomentModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: WeatherApiInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
