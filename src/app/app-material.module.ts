import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatToolbarModule
    } from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [MatToolbarModule, MatInputModule, MatAutocompleteModule, MatCardModule, MatButtonModule, MatProgressSpinnerModule],
    exports: [MatToolbarModule, MatInputModule, MatAutocompleteModule, MatCardModule, MatButtonModule, MatProgressSpinnerModule]
})

export class MaterialExportModule { }
