import { Component, OnInit } from '@angular/core';
import { CryptoService } from './../../services/crypto/crypto.service';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.styl']
})
export class CryptoComponent implements OnInit {

  cryptos: any[] = [];
  constructor(private service: CryptoService) {
    this.service.getListCrypto().subscribe((response: any) => {
      this.cryptos = response;
    });
   }

  ngOnInit() {

  }

}
