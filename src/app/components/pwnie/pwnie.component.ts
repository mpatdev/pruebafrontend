import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-pwnie',
  templateUrl: './pwnie.component.html',
  styleUrls: ['./pwnie.component.styl']
})
export class PwnieComponent implements OnInit {

  size = new FormControl('');
  actualCollection = new FormControl('');
  lost;
  constructor() { }

  ngOnInit() {
  }

  private getRealList(size) {
    const realList = [];
    for (let i = 1; i <= size; i++) {
        realList.push(i.toString());
    }
    return realList;
  }

  private findLost() {
    const actualList = this.actualCollection.value.split(',');
    const realList = this.getRealList(this.size.value);
    const lostItem = realList.filter(x => !actualList.includes(x))
            .concat(actualList.filter(x => !realList.includes(x)));
    this.lost = lostItem[0];
  }

}
