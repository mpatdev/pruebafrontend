import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwnieComponent } from './pwnie.component';

describe('PwnieComponent', () => {
  let component: PwnieComponent;
  let fixture: ComponentFixture<PwnieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PwnieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwnieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
