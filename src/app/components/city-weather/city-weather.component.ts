import { ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { WeatherService } from './../../services/weather/weather.service';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.styl']
})
export class CityWeatherComponent implements OnInit {
  id: number;
  cityId: Observable<string>;
  weather: any;
  historyList: any;
  constructor(private activatedRoute: ActivatedRoute, private weatherService: WeatherService) {
    this.cityId = this.activatedRoute.paramMap.pipe(map((p: ParamMap) => {
      return p.get('id');
    }));
    this.cityId.subscribe((id) => {
      this.id = Number(id);
      this.getWeather();
      this.getHistory();
    });
  }
  ngOnInit() {

  }

  getWeather() {
    const id = this.id;
    this.weatherService.getWeather(id).subscribe((response: any) => {
      this.weather = response;
    });
  }

  getHistory() {
    const id = this.id;
    this.weatherService.getHistoric(id).subscribe((response: any) => {
      this.historyList = response;
    });
  }
}
