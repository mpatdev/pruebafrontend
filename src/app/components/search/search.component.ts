import { CitiesService } from '../../services/cities/cities.service';
import { City } from './../../interfaces/city';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.styl']
})
export class SearchComponent implements OnInit {

  citySearch = new FormControl('');
  searchSubscription: Subscription;
  citiesList: Observable<City>;

  constructor(private cities: CitiesService, private router: Router) { }

  ngOnInit() {
  }

  search() {
    const name = this.citySearch.value;
    if (name.length < 3) {
      return;
    }
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
    this.searchSubscription = this.cities.searchCity(name).subscribe((response) => {
      this.citiesList = of(response);
    });
  }

  goWeather(id: number) {
    this.router.navigate(['weather-in-city', id]);
  }

}
